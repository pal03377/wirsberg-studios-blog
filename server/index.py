import os
import re
import time
import hashlib
import difflib
import smtplib
import datetime
import threading
import email.mime.text
import email.mime.multipart
from email.utils import formatdate, make_msgid
from urllib.parse import urlparse
import requests
import dataset
import jinja2
from flask import Flask, Response, abort, g, redirect, request, render_template, send_from_directory, json
from flask_htmlmin import HTMLMIN
from pyquery import PyQuery as pq

app = Flask(__name__)

try:
    with open('config.json') as configFile:
        app.conf = json.load(configFile)
except FileNotFoundError:
    print('Config not found, please create config.json (use config.example.json as template)')
    exit()

app.staticFileHashes = {}
for file in ['index.js', 'index.css']:
    with open(os.path.join('static', file), 'r') as f:
        app.staticFileHashes[file] = hashlib.md5(f.read().encode('utf-8')).hexdigest()

app.secret_key = app.conf['secret_key']
app.jinja_env.globals['config'] = app.conf
app.config['MINIFY_PAGE'] = not app.conf['debug']
app.config['TEMPLATES_AUTO_RELOAD'] = app.conf['debug']

if not app.conf["debug"]:
    HTMLMIN(app)

app.config['SQLALCHEMY_POOL_SIZE'] = app.conf['sqlalchemy']['poolSize']
app.config['SQLALCHEMY_POOL_RECYCLE'] = app.conf['sqlalchemy']['poolRecycle']

app.config['MAX_CONTENT_LENGTH'] = app.conf['maxContentLength']

app.newsAboRequests = {}
app.userDataCache = {}

os.chdir(os.path.dirname(os.path.realpath(__file__)))

app.db = dataset.connect(app.conf['dbConnect'])
app.dbArticles = app.db['articles']
app.dbCategories = app.db['categories']
app.dbNewsReceivers = app.db['newsReceivers']
app.dbNewsletterByCategory = app.db['newsletterByCategory']


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in [x.lower() for x in app.conf["allowed_upload_extensions"]]


def findOutStartAndEndForPage(content_length, page=0):
    start_index = app.conf['maxThingsPerPage'] * page
    if start_index >= content_length:
        start_index = -app.conf['maxThingsPerPage']
    if start_index <= -content_length:
        start_index = 0
    end_index = app.conf['maxThingsPerPage'] * (page + 1)
    if end_index > content_length:
        end_index = content_length
    if end_index <= -content_length:
        end_index = 0
    return start_index, end_index


def getAvailableId(table):
    latest = table.find_one(order_by='-id')
    if latest is None:
        return 1
    return latest['id'] + 1  # Get heighest id and add one


def log(msgType, data, user=None):
    if not user:
        user = g.user

    date = int(time.time())

    with open(app.conf['logfile'], 'a') as logFile:
        logFile.write(json.dumps({'date': date, 'user': g.user, 'type': msgType, 'data': data}) + '\n')


def getPrevImg(text, extern):
    if text:
        articleText = pq(text)
        prevImgQp = articleText('img, .fotorama a')
        if prevImgQp.attr('data-src'):
            prevImg = prevImgQp.attr('data-src')
            lazyPrevImg = prevImgQp.attr('src')
        else:
            prevImg = prevImgQp.attr('href')
            lazyPrevImg = prevImgQp.attr('data-thumb')

        if prevImg and extern and prevImg[:5] == '/img/':
            prevImgUrl = app.conf['blogServerAddress'] + prevImg
        else:
            prevImgUrl = prevImg

        if lazyPrevImg and extern and lazyPrevImg[:5] == '/img/':
            lazyPrevImgUrl = app.conf['blogServerAddress'] + lazyPrevImg
        else:
            lazyPrevImgUrl = lazyPrevImg

        return prevImgUrl, lazyPrevImgUrl
    else:
        return None, None


def xstr(s):
    if s is None:
        return ''
    return str(s)


@app.route('/refreshCompleteSearchIndex')
def refreshCompleteSearchIndex():
    if not userRoleMin('admin'):
        abort(403)
    if not request.args.get('skip_removal'):
        for article in app.dbArticles:
            removeFromSearchIndex(article["id"])
    for article in app.dbArticles:
        addToSearchIndex(article["id"])
    return "refreshed the search index for articles"


def addToSearchIndex(articleId):
    if not app.conf['prodMode']:
        return False

    article = app.dbArticles.find_one(id=articleId)

    if not article:
        return False

    prevImgUrl, lazyPrevImgUrl = getPrevImg(article['text'], True)

    articleText = pq(article['text'])
    articleText('style').remove()
    articleText('script').remove()
    text = articleText.text()

    indexRequest = requests.post(app.conf['dataServerAddress'] + '/index/edit/' + app.conf['communicationSecret'], data={
        'sourceType': 'b',
        'sourceID': article['id'],
        'slug': article['slug'],
        'creationDate': article['date'],
        'previewURL': xstr(prevImgUrl),
        'lazyPreviewURL': xstr(lazyPrevImgUrl),
        'title': article['headline'],
        'subtitle': article['subline'],
        'text': text,
        'hidden': article['hidden'],
        'private': article['private']
    })

    return indexRequest.status_code == 200


def removeFromSearchIndex(articleId):
    if not app.conf['prodMode']:
        return False

    indexRequest = requests.post(app.conf['dataServerAddress'] + '/index/remove/' + app.conf['communicationSecret'], data={
        'sourceType': 'b',
        'sourceID': articleId,
    })

    return indexRequest.status_code == 200


def render_template_outOfScope(template_name, **template_vars):
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader('templates')
    )
    template = env.get_template(template_name)
    return template.render(**template_vars)


def sendMail(subjectTemplate, recieverList, templateData, listUnsubscribe=True, bodyTemplateName='mail',
             messageIdString=None, host=app.conf['mail']['host'], port=app.conf['mail']['port'],
             name=app.conf['mail']['name'], password=app.conf['mail']['password'], address=app.conf['mail']['address']):
    server = smtplib.SMTP_SSL(host=host, port=port)
    server.set_debuglevel(app.conf['debug'])
    server.login(name, password)
    for reciever in recieverList:
        plain = render_template_outOfScope(bodyTemplateName + '.txt', user=reciever, config=app.conf, **templateData)
        html = render_template_outOfScope(bodyTemplateName + '.html', user=reciever, config=app.conf, **templateData)

        message = email.mime.multipart.MIMEMultipart('alternative')
        message['From'] = app.conf['blogName'] + ' <' + address + '>'
        message['To'] = reciever['name'] + ' <' + reciever['email'] + '>'
        message['Subject'] = jinja2.Template(subjectTemplate).render(user=reciever, config=app.conf, **templateData)
        message['Date'] = formatdate(localtime=True)
        message['Message-ID'] = make_msgid(messageIdString, host)
        # if listUnsubscribe:  # Not implemented, because it only works if the sender has a high enough reputuation :unamused:
        #     message['List-Unsubscribe'] = '<{url}/deactivateNews/{rmToken}>'.format(url=app.conf['blogServerAddress'], rmToken=reciever['rmToken'], mailAddr=app.conf['mail']['address'])  # '<mailto:{mailAddr}?subject=unsubscribe:{rmToken}>, '
        message.attach(email.mime.text.MIMEText(plain, 'plain'))
        message.attach(email.mime.text.MIMEText(html, 'html'))

        server.sendmail(address, reciever['email'], message.as_string())
    server.close()


def getUserData(token=None, forceRequest=False):
    if not token:
        token = request.cookies.get('token')

    if not token:
        return None

    if token not in app.userDataCache or app.userDataCache[token]['expireOn'] > time.time() or forceRequest:
        # Cache aktualisieren
        userDataRequest = requests.post(app.conf['dataServerAddress'] + '/getUserData/' + app.conf['communicationSecret'], data={'token': token})
        userDataResponse = userDataRequest.json()
        if userDataRequest.status_code != 200:
            if userDataResponse['error'] == 'user_not_found':
                return False
            return None
        app.userDataCache[token] = userDataRequest.json()
        app.userDataCache[token]['expireOn'] = time.time() + app.conf['maxUserDataCacheAge']

        if app.dbNewsReceivers.find_one(userId=app.userDataCache[token]['id']):
            app.dbNewsReceivers.update(dict(userId=app.userDataCache[token]['id'], name=app.userDataCache[token]['name'], email=app.userDataCache[token]['email']), ['userId'])

    return app.userDataCache[token]


def userRoleMin(role, user=None):
    if not user:
        user = g.user

    if not user:
        return False
    return int(user['role']) >= app.conf['roles'][role]


def getNewsActiveCategories(userId):
    newsActiveCategories = [category['categoryId'] for category in app.dbNewsletterByCategory.find(userId=userId)]

    activeCategories = {}
    if -1 not in newsActiveCategories:
        for category in newsActiveCategories:
            if category == 0:
                activeCategories['0'] = app.conf['restCategorieName']  # restCategory
            else:
                category = app.dbCategories.find_one(id=category)
                activeCategories[category['slug']] = category['name']
    else:
        activeCategories = {'-1': 'Alles'}

    return activeCategories


def getHeaderData():
    response = {}

    response['categories'] = [{'id': 0, 'name': app.conf['restCategorieName'], 'slug': '0'}]
    response['categories'] += list(app.dbCategories.find())

    for category in response['categories']:
        category['count'] = len(list(app.dbArticles.find(categoryId=category['id'])))

    response['allCount'] = len(list(app.dbArticles.find()))

    response['user'] = g.user

    if g.user and app.dbNewsReceivers.find_one(userId=g.user['id']):
        response['newsRmToken'] = app.dbNewsReceivers.find_one(userId=g.user['id'])['rmToken']

        response['newsActiveCategories'] = list(getNewsActiveCategories(g.user['id']).keys())

    return response


def timestampToIso(timestamp):
    return datetime.datetime.fromtimestamp(timestamp).isoformat()


def getInitials(user):
    name = user['name'].split(' ')

    if len(name) <= 1:
        out = name[0][:1]
    else:
        out = name[0][:1] + name[1][:1]

    return out.upper()


def tstampToReadable(date):
    return datetime.datetime.fromtimestamp(date).strftime('%d.%m.%Y %H:%M')


app.jinja_env.globals['userRoleMin'] = userRoleMin
app.jinja_env.globals['getHeaderData'] = getHeaderData
app.jinja_env.globals['len'] = len
app.jinja_env.globals['max'] = max
app.jinja_env.globals['json'] = json
app.jinja_env.globals['list'] = list
app.jinja_env.globals['difflib'] = difflib
app.jinja_env.globals['timestampToIso'] = timestampToIso
app.jinja_env.globals['getInitials'] = getInitials
app.jinja_env.globals['staticFileHashes'] = app.staticFileHashes
app.jinja_env.globals['tstampToReadable'] = tstampToReadable


@app.errorhandler(404)
def error_notFound(e):
    return render_template('404.html'), 404


@app.errorhandler(403)
def error_accessDenied(e):
    return render_template('403.html'), 403


@app.errorhandler(500)
def error_ServerError(e):
    return render_template('500.html'), 500


@app.before_request
def before_request():
    g.user = getUserData()

    if g.user is False:
        return redirect('{}/autoregister?redirect={}'.format(app.conf['videoPlatformLink'], request.path), code=307)

    return None


@app.route('/')
def index():
    article_criteria = dict(order_by='-date', _limit=app.conf['maxThingsPerPage'] + 1)
    if not userRoleMin('blogger'):
        article_criteria.setdefault("hidden", 0)
    if not userRoleMin('user'):
        article_criteria.setdefault("private", 0)

    articles = list(app.dbArticles.find(**article_criteria))

    for article in articles:
        article['category'] = app.dbCategories.find_one(id=article['categoryId'])

        article['prevImg'], article['lazyPrevImg'] = getPrevImg(article['text'], False)

    if not articles:
        article = None

        category = None
    else:
        article = articles[0]

        category = app.dbCategories.find_one(id=article['categoryId'])

    more = len(articles) > app.conf['maxThingsPerPage']

    return render_template('index.html', article=article, category=category, articles=articles[1:app.conf['maxThingsPerPage']], more=more)


@app.route('/articles')
@app.route('/articles/<int:page>')
def articleList(page=1):
    return categoryArticleList(None, page)


@app.route('/category/<category>')
@app.route('/category/<category>/<int:page>')
def categoryArticleList(category, page=1):
    """Get all articles in <category> listed as HTML. Use <category>="all" to list all articles."""
    page = max(0, page-1)  # 0-indexing => damit Pages bei 1 anfangen, wird dieser Trick verwendet

    if category and not app.dbCategories.find_one(slug=category) and category != 'all':
        abort(404)

    article_criteria = dict(order_by='-date')
    if category:
        if category == 'all':
            categoryDict = {'slug': 'all'}
        else:
            categoryDict = app.dbCategories.find_one(slug=category)
            article_criteria.setdefault('categoryId', categoryDict['id'])
    else:
        categoryDict = {'slug': 'others', 'name': app.conf['restCategorieName']}
        article_criteria.setdefault('categoryId', 0)
    if not userRoleMin('blogger'):
        article_criteria.setdefault('hidden', 0)
    if not userRoleMin('user'):
        article_criteria.setdefault('private', 0)

    articles = list(app.dbArticles.find(**article_criteria))

    start_index, end_index = findOutStartAndEndForPage(len(articles), page)

    maxPage = int(len(articles) / app.conf['maxThingsPerPage'])

    if maxPage < page and page != 0:
        if category:
            return redirect('category/' + category + '/' + str(maxPage + 1))
        return redirect('articles/' + str(maxPage + 1))

    for article in articles[start_index:end_index]:
        article['category'] = app.dbCategories.find_one(id=article['categoryId'])

        article['prevImg'], article['lazyPrevImg'] = getPrevImg(article['text'], False)

    return render_template('articles.html', category=categoryDict, articles=articles[start_index:end_index], page=page+1, maxPage=maxPage+1)


@app.route('/article/<slug>')
@app.route('/<amp>/article/<slug>')
def returnArticle(slug, amp=None):
    article = app.dbArticles.find_one(slug=slug)

    if not article:
        abort(404)

    if article['hidden'] and not userRoleMin('blogger'):
        abort(403)
    if article['private'] and not userRoleMin('user'):
        abort(403)

    article['category'] = app.dbCategories.find_one(id=article['categoryId'])

    prevImg = getPrevImg(article['text'], False)[0]

    if amp == "m":
        if 'TelegramBot' in request.headers.get('User-Agent'):
            return redirect('/article/{}'.format(slug), code=307)
        return render_template('amp_article.html', article=article, prevImg=prevImg)
    elif amp is None:
        return render_template('article.html', article=article, prevImg=prevImg)
    return abort(404)


@app.route('/img/<int:articleId>/<name>')
def getImg(articleId, name):
    article = app.dbArticles.find_one(id=articleId)

    if not article and not userRoleMin('blogger'):
        abort(404)

    if article:
        if article['hidden'] and not userRoleMin('blogger') or article['private'] and not userRoleMin('user'):
            abort(403)

    if not os.path.isfile(os.path.join(app.conf['upload_folder'], str(articleId), name)):
        abort(404)

    response = send_from_directory(os.path.join(app.conf['upload_folder'], str(articleId)), name)
    response.cache_control.max_age = app.conf['imgMaxAge']

    return response


@app.route('/rss')
def rss():
    url = urlparse(request.url)
    url = '{}://{}'.format(url.scheme, url.netloc)

    if userRoleMin('user'):
        canSeePrivate = {}
    else:
        canSeePrivate = {'private': False}

    if request.args.get('c'):
        categoryIds = []
        for category in request.args['c'].split(','):
            if category == '0':
                category = {'id': 0}
            else:
                category = app.dbCategories.find_one(slug=category)

            if category:
                categoryIds.append(category['id'])

        categoryIds = [e for e in categoryIds if e not in ('item', 5)]

        articles = list(app.dbArticles.find(order_by='-date', _limit=10, hidden=False, categoryId=categoryIds, **canSeePrivate))
    else:
        articles = list(app.dbArticles.find(order_by='-date', _limit=10, hidden=False, **canSeePrivate))

    for article in articles:
        article['date'] = datetime.datetime.fromtimestamp(article['date']).isoformat('T') + 'Z'

        article['prevImg'], article['lazyPrevImg'] = getPrevImg(article['text'], False)

    if articles:
        updated = articles[0]['date']
    else:
        updated = datetime.datetime.fromtimestamp(0).isoformat('T') + 'Z'

    return Response(render_template('rss.xml', url=url, selfeUrl=request.url, updated=updated, articles=articles), mimetype='application/atom+xml; charset=utf-8')


@app.route('/updateUser/<communicationSecret>', methods=['POST'])
def updateUser(communicationSecret):
    if communicationSecret != app.conf['communicationSecret']:
        abort(403)
    getUserData(request.form["token"], True)
    return 'success'


@app.route('/getArticlesToIndex/<communicationSecret>')
def getArticlesToIndex(communicationSecret):
    if communicationSecret != app.conf['communicationSecret']:
        abort(403)

    for article in app.dbArticles.find(hidden=False, private=False):
        threading.Thread(target=addToSearchIndex, args=[article['id']]).start()

    return json.dumps({'success': True})


@app.route('/editArticle')
@app.route('/editArticle/<int:articleId>')
def editArticle(articleId=None):
    if not userRoleMin('blogger'):
        abort(403)

    if not articleId:
        return redirect('/editArticle/' + str(getAvailableId(app.dbArticles)))

    article = app.dbArticles.find_one(id=articleId)

    if not article and articleId != getAvailableId(app.dbArticles):
        return redirect('/editArticle/' + str(getAvailableId(app.dbArticles)))

    if article:
        if not article['hidden'] and not userRoleMin('publisher'):
            abort(403)

        new = False
    else:
        article = {
            'id': articleId,
            'date': int(time.time()),
            'author': g.user['name'],
            'categoryId': None,
            'hidden': True,
            'private': False
        }

        new = True

    categories = list(app.dbCategories.find())

    categories.insert(0, {'id': 0, 'name': app.conf['restCategorieName']})

    return render_template('editArticle.html', article=article, categories=categories, new=new)


@app.route('/editArticle/<int:articleId>/uploadImg', methods=["POST"])
def uploadArticleImg(articleId):
    if not userRoleMin('blogger'):
        abort(403)

    article = app.dbArticles.find_one(id=articleId)

    if not article and getAvailableId(app.dbArticles) != articleId:
        abort(404)

    if article and not article['hidden'] and not userRoleMin('publisher'):
        abort(403)

    if 'file' not in request.files:
        abort(400)

    img = request.files['file']

    if img.filename == '':
        return abort(400)

    if not img or not allowed_file(img.filename):
        abort(400)

    filenameFront = hashlib.md5(img.read()).hexdigest()
    filenameEnd = img.filename[img.filename.rfind('.'):].lower()
    filename = filenameFront + filenameEnd

    if not os.path.exists(os.path.join(app.conf['upload_folder'], str(articleId))):
        os.mkdir(os.path.join(app.conf['upload_folder'], str(articleId)))

    img.seek(0)
    img.save(os.path.join(app.conf['upload_folder'], str(articleId), filename))

    for size in ['20', '100', '1024', '1920']:
        os.popen('ffmpeg -y -i {} -vf scale={}:-1 {} 2>&1'.format(os.path.join(app.conf['upload_folder'], str(articleId), filename),
                                                                  size, os.path.join(app.conf['upload_folder'], str(articleId), '{}-{}.jpg'.format(filenameFront, size)))).read()

    if article:
        log('addImg', {'name': filename, 'article': article})
    else:
        log('addImg', {'name': filename, 'article': {'id': articleId}})

    return json.dumps({'filename': '/img/' + str(articleId) + '/' + filename})


@app.route('/ajax/saveArticle/<int:articleId>', methods=['POST'])
def saveArticle(articleId=None):
    if not userRoleMin('blogger'):
        abort(403)

    if isinstance(request.get_data(), (bytes, bytearray)):
        data = json.loads(request.get_data().decode())
    else:
        data = json.loads(request.get_data())

    response = {}
    response['error'] = {}

    article = app.dbArticles.find_one(id=articleId)

    if article and not article['hidden'] and not userRoleMin('publisher'):
        abort(403)

    for key in ['slug', 'headline', 'subline', 'text', 'textAmp', 'textMd', 'author', 'date', 'categoryId', 'hidden', 'private']:
        if key not in data:
            response['error'][key] = 'Fehlt'
        elif key == 'slug':
            articleSlugCollisionTest = app.dbArticles.find_one(slug=data[key])
            if not re.match(r'^[a-zA-Z0-9_-]{1,50}$', data[key]):
                response['error'][key] = 'Darf nur die Zeichen a-z, A-Z, 0-9, _, - beinhalten und maximal 50 Zeichen lang sein'
            elif articleSlugCollisionTest and articleSlugCollisionTest['id'] != articleId or data[key] == 'all' or data[key] == 'other':
                response['error'][key] = 'Existiert schon'
        elif key == 'categoryId':
            if data[key] != 0 and not app.dbCategories.find_one(id=data[key]):
                response['error'][key] = 'Existiert nicht'
        elif key == 'date':
            if not isinstance(data[key], int):
                response['error'][key] = 'Muss eine Zahl sein'
        elif key in ['hidden', 'private']:
            if not isinstance(data[key], bool):
                response['error'][key] = 'Muss ein Boolean sein'
            elif key == 'hidden' and data[key] is False and not userRoleMin('publisher'):
                response['error'][key] = 'Nicht genügend Rechte um zu veröffentlichen'

    imgDir = os.path.join(app.conf['upload_folder'], str(articleId))

    response['removedImgs'] = []

    response['success'] = len(response['error']) == 0

    data['id'] = articleId

    if response['success']:
        if os.path.exists(imgDir):
            for fileName in os.listdir(imgDir):
                if fileName[:fileName.rfind('.')] not in data['textMd'] and '-' not in fileName:
                    os.remove(os.path.join(imgDir, fileName))
                    for size in ['20', '100', '1024', '1920']:
                        if os.path.exists(os.path.join(imgDir, fileName[:fileName.rfind('.')] + '-' + size + '.jpg')):
                            os.remove(os.path.join(imgDir, fileName[:fileName.rfind('.')] + '-' + size + '.jpg'))

                    response['removedImgs'].append('/img/' + fileName)

                    log('rmImg', {'name': fileName, 'article': article})

            if not os.listdir(imgDir):
                os.removedirs(imgDir)

        if article:
            app.dbArticles.update(data, ['id'])
        else:
            del data['id']
            app.dbArticles.insert(data)
            data['id'] = app.dbArticles.find_one(slug=data['slug'])['id']

        threading.Thread(target=addToSearchIndex, args=[data['id']]).start()

        if data != article:
            if not article:
                article = {'slug': '', 'headline': '', 'subline': '',
                           'text': '', 'textAmp': '', 'textMd': '',
                           'author': '', 'date': 0, 'categoryId': 0,
                           'hidden': True, 'private': True, 'id': data['id']}
            dataBefore = article
            dataAfter = data
            for dataBeforeAfter in [dataBefore, dataAfter]:
                if dataBeforeAfter['categoryId'] == 0:
                    dataBeforeAfter['category'] = {'id': 0, 'slug': None, 'name': app.conf['restCategorieName']}
                else:
                    dataBeforeAfter['category'] = dict(app.dbCategories.find_one(id=dataBeforeAfter['categoryId']))

            log('editArticle', {'dataBefore': dataBefore, 'dataAfter': dataAfter})

            if bool(dataBefore['hidden']) is True and dataAfter['hidden'] is False:
                prevImg = getPrevImg(article['text'], True)[0]

                threading.Thread(target=sendMail, kwargs={
                    'subjectTemplate': 'Neuer Artikel: {{article[\'headline\']}}',
                    'recieverList': list(app.dbNewsReceivers.find(userId=[user['userId'] for user in app.dbNewsletterByCategory.find(categoryId=[data['categoryId'], -1])])),
                    'templateData': {'article': data, 'prevImg': prevImg},
                    'listUnsubscribe': True
                }).start()

    return json.dumps(response)


@app.route('/ajax/removeArticle/<int:articleId>')
def removeArticle(articleId):
    if not userRoleMin('blogger'):
        abort(403)

    article = app.dbArticles.find_one(id=articleId)

    if not article:
        abort(404)

    if not article['hidden'] and not userRoleMin('publisher'):
        abort(403)

    app.dbArticles.delete(id=articleId)

    removedImgs = []
    if os.path.exists(os.path.join(app.conf['upload_folder'], str(articleId))):
        for fileName in os.listdir(os.path.join(app.conf['upload_folder'], str(articleId))):
            if '-' not in fileName:
                os.remove(os.path.join(app.conf['upload_folder'], str(articleId), fileName))
                for size in ['20', '100', '1024', '1920']:
                    if os.path.exists(os.path.join(app.conf['upload_folder'], str(articleId), fileName[:fileName.rfind('.')] + '-' + size + '.jpg')):
                        os.remove(os.path.join(app.conf['upload_folder'], str(articleId), fileName[:fileName.rfind('.')] + '-' + size + '.jpg'))

            removedImgs.append('/img/' + fileName)

            log('rmImg', {'name': fileName, 'article': article})
        os.removedirs(os.path.join(app.conf['upload_folder'], str(articleId)))

    dataBefore = article
    if dataBefore['categoryId'] == 0:
        dataBefore['category'] = {'id': 0, 'slug': None, 'name': app.conf['restCategorieName']}
    else:
        dataBefore['category'] = dict(app.dbCategories.find_one(id=dataBefore['categoryId']))

    threading.Thread(target=removeFromSearchIndex, args=[articleId]).start()

    log('rmArticle', {'dataBefore': article})

    return json.dumps({'success': True})


@app.route('/editCategory')
@app.route('/editCategory/<int:categoryId>')
def editCategory(categoryId=None):
    if not userRoleMin('publisher'):
        abort(403)

    if not categoryId:
        return redirect('/editCategory/' + str(getAvailableId(app.dbCategories)))

    category = app.dbCategories.find_one(id=categoryId)

    if not category and categoryId != getAvailableId(app.dbCategories):
        return redirect('/editCategory/' + str(getAvailableId(app.dbCategories)))

    articles = list(app.dbArticles.find(id=categoryId))

    if category:
        new = False
    else:
        category = {
            'id': getAvailableId(app.dbCategories),
            'slug': '',
            'name': '',
            'subline': ''
        }

        new = True

    return render_template('editCategory.html', category=category, articles=articles, new=new)


@app.route('/ajax/saveCategory/<int:categoryId>', methods=['POST'])
def saveCategory(categoryId):
    if not userRoleMin('publisher'):
        abort(403)

    if isinstance(request.get_data(), (bytes, bytearray)):
        data = json.loads(request.get_data().decode())
    else:
        data = json.loads(request.get_data())

    response = {}
    response['error'] = {}

    category = app.dbCategories.find_one(id=categoryId)

    for key in ['slug', 'name']:
        if key not in data:
            response['error'][key] = 'Fehlt'
        elif key == 'slug':
            categorySlugCollisionTest = app.dbCategories.find_one(slug=data[key])
            if not re.match(r'^[a-zA-Z0-9_-]{1,50}$', data[key]):
                response['error'][key] = 'Darf nur die Zeichen a-z, A-Z, 0-9, _, - beinhalten und maximal 50 Zeichen lang sein'
            elif categorySlugCollisionTest and categorySlugCollisionTest['id'] != categoryId:
                response['error'][key] = 'Existiert schon'
            elif data[key] in ['all', '0', '-1']:
                response['error'][key] = 'unzulässig'

    response['success'] = len(response['error']) == 0

    data['id'] = categoryId

    if response['success']:
        if category:
            app.dbCategories.update(data, ['id'])
        else:
            del data['id']
            app.dbCategories.insert(data)
            data['id'] = app.dbCategories.find_one(slug=data['slug'])['id']

        if data != category:
            category = {'slug': '', 'name': ''}
            log('editCategory', {'response': response, 'dataBefore': category, 'dataAfter': data})

    return json.dumps(response)


@app.route('/ajax/removeCategory/<int:categoryId>')
def removeCategory(categoryId):
    if not userRoleMin('publisher'):
        abort(403)

    category = app.dbCategories.find_one(id=categoryId)

    if not category:
        abort(404)

    for article in app.dbArticles.find(categoryId=categoryId):
        removedImgs = []
        if os.path.exists(os.path.join(app.conf['upload_folder'], str(categoryId))):
            for fileName in os.listdir(os.path.join(app.conf['upload_folder'], str(categoryId))):
                os.remove(os.path.join(app.conf['upload_folder'], str(categoryId), fileName))

                removedImgs.append('/img/' + fileName)
            os.removedirs(os.path.join(app.conf['upload_folder'], str(categoryId)))

        app.dbArticles.delete(id=article['id'])

        log('rmArticle', {'removedImgs': removedImgs, 'dataBefore': article})

    app.dbCategories.delete(id=categoryId)

    log('rmCategory', {'dataBefore': category})

    return json.dumps({'success': True})


@app.route('/logView')
@app.route('/logView/<int:page>')
def logView(page=1):
    if not userRoleMin('publisher'):
        abort(403)

    page = max(0, page-1)  # 0-indexing => damit Pages bei 1 anfangen, wird dieser Trick verwendet

    try:
        with open(app.conf['logfile'], 'r') as logFile:
            lines = logFile.readlines()[::-1]
    except FileNotFoundError:
        lines = []

    start_index, end_index = findOutStartAndEndForPage(len(lines), page)

    maxPage = int(len(lines) / app.conf['maxThingsPerPage'])

    if maxPage < page and page != 0:
        return redirect('logView/' + str(maxPage + 1))

    if not lines:
        return render_template('logView.html', data=None, page=page+1, maxPage=maxPage+1)

    data = []
    for line in lines[start_index:end_index]:
        data.append(json.loads(line))

    return render_template('logView.html', data=data, page=page+1, maxPage=maxPage+1)


@app.route('/ajax/requestNewsAbo')
def requestNewsAbo():
    if not userRoleMin('user'):
        return json.dumps({'success': False, 'error': 'Nicht angemeldet'}), 400

    if app.dbNewsReceivers.find_one(userId=g.user['id']):
        return json.dumps({'success': False, 'error': 'Newsletter schon abonniert'}), 400

    for key in list(app.newsAboRequests):
        if app.newsAboRequests[key]['user']['id'] == g.user['id']:
            app.newsAboRequests.pop(key)

    token = hashlib.sha256(os.urandom(500)).hexdigest()

    app.newsAboRequests[token] = {}
    app.newsAboRequests[token]['user'] = g.user
    app.newsAboRequests[token]['expireOn'] = time.time() + app.conf['maxNewsAboRequestAge']

    threading.Thread(target=sendMail, kwargs={
        'subjectTemplate': 'Newsletter Bestätigung',
        'bodyTemplateName': 'mailConfirmation',
        'recieverList': [{'email': g.user['email'], 'name': g.user['name']}],
        'templateData': {'token': token},
        'listUnsubscribe': False
    }).start()

    return json.dumps({'success': True})


@app.route('/confirmNews/<token>')
def confirmationNewsletter(token):
    if token not in app.newsAboRequests:
        return render_template('confirmNews.html', state='invalid')

    if app.newsAboRequests[token]['expireOn'] < time.time():
        app.newsAboRequests.pop(token)

        return render_template('confirmNews.html', state='timedOut')

    user = app.newsAboRequests[token]['user']

    if app.dbNewsReceivers.find_one(userId=user['id']):  # Shouldn't be necessary, but safety is always good practice.
        abort(400)

    rmToken = hashlib.sha256(os.urandom(500)).hexdigest()

    app.dbNewsReceivers.insert(dict(userId=user['id'], name=user['name'], email=user['email'], rmToken=rmToken))

    app.newsAboRequests.pop(token)

    if not app.dbNewsletterByCategory.find_one(userId=user['id']):
        app.dbNewsletterByCategory.insert({'userId': user['id'], 'categoryId': -1})

    activeCategories = getNewsActiveCategories(user['id'])

    return render_template('confirmNews.html', state='success', activeCategories=activeCategories, rmToken=rmToken)


@app.route('/ajax/setNews', methods=['POST'])
def setNewsAjax():
    token = request.form['token']
    toActivateCategories = request.form.getlist('toActivateCategories[]')

    if not token:
        abort(400)

    if not app.dbNewsReceivers.find_one(rmToken=token):
        return json.dumps({'success': False, 'error': 'Token ungültig'}), 403

    userId = app.dbNewsReceivers.find_one(rmToken=token)['userId']

    app.dbNewsletterByCategory.delete(userId=userId)

    if '-1' in toActivateCategories:
        app.dbNewsletterByCategory.insert({'userId': userId, 'categoryId': -1})
    else:
        for categorySlug in toActivateCategories:
            if categorySlug == '0':
                category = {'id': 0}
            else:
                category = app.dbCategories.find_one(slug=categorySlug)

            if category:
                app.dbNewsletterByCategory.insert({'userId': userId, 'categoryId': category['id']})

    return json.dumps({'success': True})


@app.route('/ajax/deactivateNews', methods=['POST'])
def deactivateNewsAjax():
    token = request.form['token']

    if not token:
        abort(400)

    if not app.dbNewsReceivers.find_one(rmToken=token):
        return json.dumps({'success': False, 'error': 'Token ungültig'}), 403

    app.dbNewsReceivers.delete(rmToken=token)

    return json.dumps({'success': True})


@app.route('/deactivateNews/<token>')
def deactivateNews(token):
    if not app.dbNewsReceivers.find_one(rmToken=token):
        return render_template('deactivateNews.html', state='invalid')

    app.dbNewsReceivers.delete(rmToken=token)

    token = hashlib.sha256(os.urandom(500)).hexdigest()

    app.newsAboRequests[token] = {}
    app.newsAboRequests[token]['user'] = g.user
    app.newsAboRequests[token]['expireOn'] = time.time() + app.conf['maxNewsAboRequestAge']

    return render_template('deactivateNews.html', state='success', token=token)


@app.route('/tea')
def tea():
    abort(418)


if __name__ == '__main__':
    app.run('127.0.0.1', port=app.conf['port'], debug=app.conf['debug'])
