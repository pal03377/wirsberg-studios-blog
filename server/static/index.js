// Firebase config

var tmp = location.host.split('.').reverse();
var domain = '.' + tmp[1] + '.' + tmp[0];
if (location.host == '127.0.0.1') {
    domain = '127.0.0.1';
}

var config = {
    apiKey: "AIzaSyAXLPZdtIuvGMSFs4YoBqVUGP_OjKtQmSs",
    authDomain: "wirsberg-studios.firebaseapp.com",
    databaseURL: "https://wirsberg-studios.firebaseio.com",
    projectId: "wirsberg-studios",
    storageBucket: "wirsberg-studios.appspot.com",
    messagingSenderId: "155152666349"
};
firebase.initializeApp(config);

var uiConfig = {
    signInOptions: [
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebase.auth.EmailAuthProvider.PROVIDER_ID
    ],
    tosUrl: 'https://wirsberg-studios.de/legal',
    'callbacks': {
        'signInSuccess': function(currentUser, credential, redirectUrl) {
            swal({
                title: 'Bitte warten',
                allowOutsideClick: false,
                onOpen: function () {
                    swal.showLoading()
                }
            })
            currentUser.getIdToken().then(function (accessToken) {
                Cookies.set("token", accessToken, {domain: domain});
                Cookies.set("localStorage", JSON.stringify(localStorage), {domain: domain});
                window.location.hash = '';
                location.reload();
            });
            return false;
        }
    }
};

// Init stuff

tryLazyload();

updateTime();
setInterval(updateTime, 60000);

reauth();
setInterval(reauth, 30000);

$('input[type="datetime-local"]').each(function (e) {
    e = $($('input[type="datetime-local"]')[e]);

    date = new Date((e.attr('timestamp') -(new Date().getTimezoneOffset() * 60)) * 1000); // Adjust timezone

    e.val(date.toISOString().substr(0, 19)); // Write without timezone
});

$('#editArticle textarea, #editCategory textarea').on('keydown keyup', function () { // https://stackoverflow.com/a/16620046/8456146
    this.style.height = '1px';
    this.style.height = (this.scrollHeight + 2) + 'px';
}).each(function () {
    this.style.height = '1px';
    this.style.height = (this.scrollHeight + 2) + 'px';
});

if (navigator.share) {
    $('#shareButton').removeClass('hidden');
}

firebase.auth().onAuthStateChanged(function (user) {
    if (location.hash == '#login' && !Boolean(firebase.auth().getUid())) {
        login();
    }

    if (user.picture || user.photoURL) {
        $('#userImg').attr('src', user.picture || user.photoURL);
    }

    $('.mdl-layout').on('mdl-componentupgraded', function(e) {
        if ($(e.target).hasClass('mdl-layout')) {
            if (location.hash == '#sendMail' && Boolean(firebase.auth().getUid()) && !user.emailVerified) {
                user.sendEmailVerification().then(function() {
                    $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Eine neue Bestätigungs Mail wurde versendet.'});
                    location.hash = '';
                }).catch(function(error) {
                    $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Die Mail konnte leider nicht versendet werden. Bitte versuche es später noch einmal.'});
                });
            }

            if (Boolean(firebase.auth().getUid()) && !user.emailVerified) {
                $('#toast')[0].MaterialSnackbar.showSnackbar({
                    message: 'Bitte bestätige deine Email-Adresse, indem du den Link in der dir zugesendeten Mail öffnest.',
                    actionHandler: function(event) {
                        location.hash = '#sendMail';
                        location.reload(true);
                    },
                    actionText: 'Erneut senden'
                });
            }
        }
    });
});

$("#search").keyup(function(evt){
    if(evt.keyCode == 13){
        search('search');
    }
});
autoCompleteElement("search");

// Functions

function share() {
    navigator.share({
        title: shareApi.text,
        url: shareApi.link
    });
}

function toggleImgContainerFullscreen(state, e=null) {
    if (state) {
        $('#imgContainerFullscreenContainer img').remove();
        $(e).find('img').attr('src', $(e).attr('data-src')).attr('srcset', null);
        $(e).find('img').clone().appendTo('#imgContainerFullscreenContainer');

        setTimeout(function () {
            $('#imgContainerFullscreenContainer img').addClass('active');
        }, 100);
    }

    $('body').toggleClass('unscrollAble', state);
    $('#imgContainerFullscreenContainer').toggleClass('active', state);
}

function updateTime() {
    $('time').each(function (e) {
        e = $($('time')[e]);

        date = new Date(e.attr('timestamp') * 1000);

        e.attr('datetime', date.toISOString());

        var months = ['Jan','Feb','Mär','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
        var days = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];
        var timeAgo = function (time) {
            var seconds = (+new Date() - +new Date(time)) / 1000;
            var timeFormats = [
                [60, 3600, 'Minute', 'Minuten'], // 60
                [3600, 86400, 'Stunde', 'Stunden'], // 60*60
                [86400, 604800, 'Tag', 'Tage'], // 60*60*24
                [604800, 2419200, 'Woche', 'Wochen'], // 60*60*24*7*4
                [2419200, 29030400, 'Monat', 'Monate'], // 60*60*24*7*4
                [29030400, Infinity, 'Jahr', 'Jahre'], // 60*60*24*7*4*12
            ];

            if (seconds < timeFormats[0][0]) {
                return '<1 Minute alt';
            }

            var timeFormatFinal;
            for (timeFormat in timeFormats) {
                timeFormatFinal = timeFormats[timeFormat];
                if (seconds < timeFormatFinal[1]) {
                    break;
                }
            }

            var value = Math.floor(seconds / timeFormatFinal[0]);
            return value + ' ' + (value == 1 ? timeFormatFinal[2] : timeFormatFinal[3]) + ' alt';
        }
        var pad = function (d) { // https://stackoverflow.com/a/5774055/8456146
            return (d < 10) ? '0' + d.toString() : d.toString();
        }

        e.html(days[date.getDay()] + ' ' +
        date.getDate() + '. ' +
        months[date.getMonth()] + ' ' +
        date.getFullYear() + ' ' +
        pad(date.getHours()) + ':' +
        pad(date.getMinutes()) + ' (' +
        timeAgo(date.getTime()) +
        ')');
    });
}

function tryLazyload() {
    try {
        lazyload();
    } catch (e) {
        console.warn("could not lazy load");
        $(".lazyload").each(function (index, elem) {
            $(elem).attr("src", $(elem).attr("data-src"));
        });
    }
}

function activateNewsletter() {
    swal({
        title: 'Newsletter abonieren',
        text: '',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Wiederholen',
        cancelButtonText: 'Abbrechen',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        showLoaderOnConfirm: true,
        onOpen: function () {
            $('.swal2-confirm').trigger('click');
        },
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    dataType: "json",
                    url: '/ajax/requestNewsAbo',
                    success: function (data, textStatus, jqxhr) {
                        if (data.success) {
                            resolve();
                        } else {
                            reject('Fehler: ' + data.error);
                        }
                    },
                    error: function (jqxhr, textStatus, errorThrown) {
                        reject('Fehler: ' + JSON.parse(jqxhr.responseText).error) + ' (' + errorThrown + ')';
                    }
                });
            })
        },
        allowOutsideClick: false
    }).then(function () {
        swal({
            type: 'success',
            title: 'Abonnement bestätigen',
            text: 'Um das Abonnement zu bestätigen musst du noch den Link in der E-Mail, die dir gerade zugeschickt wurde öffnen. Unbedingt auch den Spam Ordner prüfen!'
        });
    }, function () {});
}

function deactivateNewsletter(token) {
    swal({
        title: 'Sicher?',
        html: 'Willst du den Newsletter wirklich deabonnieren?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja',
        cancelButtonText: 'Nein',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    dataType: "json",
                    url: '/ajax/deactivateNews',
                    method: 'POST',
                    data: {'token': token},
                    success: function (data, textStatus, jqxhr) {
                        if (data.success) {
                            resolve();
                        } else {
                            reject('Fehler: ' + data.error);
                        }
                    },
                    error: function (jqxhr, textStatus, errorThrown) {
                        reject('Fehler: ' + JSON.parse(jqxhr.responseText).error + ' (' + errorThrown + ')');
                    }
                });
            })
        },
        allowOutsideClick: false
    }).then(function () {
        swal({
            type: 'success',
            title: 'Deabonniert',
            html: 'Du wirst den Newsletter nun nicht mehr erhalten.'
        }).then(function () {
            location.reload();
        }, function () {
            location.reload();
        });
    }, function () {});
}

function loadImg(url) {
    var img = new Image();
    img.src = url;
    if (img.complete) {
        return new Promise(function(resolve, reject) {
            resolve(img);
        });
    } else {
        return new Promise(function(resolve, reject) {
            img.onload = function() { resolve(img) };
            img.onerror = function() { reject(new Error(img.src)) };
        });
    }
}

function mdRender(text) {
    var $result = $('<div/>', {
        html: simplemde.markdown(text)
            .replace(/src/g, '_src_')});

    $result.find('img').each(function (e) {
        var e = $($result.find('img').get(e));

        e.addClass('lazyload');

        var src = e.attr('_src_');

        if (src.substr(0, 5) == '/img/') {
            var filename = src.substr(0, src.lastIndexOf('.'));

            e.attr('data-src', filename + '-1920.jpg');
            e.attr('data-srcset', filename + '-20.jpg 20w, ' + filename + '-100.jpg 100w, ' + filename + '-1024.jpg 1024w, ' + filename + '-1920.jpg 1920w');
            e.attr('_src_', filename + '-20.jpg');
        } else {
            e.attr('data-src', src);
            e.attr('_src_', src);
        }
    });

    $result.find('gallery > img').each(function() {
        var newElement = $("<a />");

        $(newElement).attr('href', $(this).attr('data-src'));

        var src = $(this).attr('data-src');
        if (src.slice(0, 5) === '/img/' && src.slice(-9) === '-1920.jpg') {
            $(newElement).attr('data-thumb', src.slice(0, -9) + '-100.jpg');
        }

        $(this).replaceWith(function () {
            return $(newElement).append($(this).contents());
        });
    });

    $result.find('img').each(function() {
        var newElement = $("<div><img/></div>")
            .addClass('imgContainer')
            .attr('onclick', 'toggleImgContainerFullscreen(true, this)');

        $.each(this.attributes, function(i, attrib){
            $(newElement).children().attr(attrib.name, attrib.value);
        });

        $(this).replaceWith(function () {
            return $(newElement).append($(this).contents());
        });
    });

    $result.find('gallery').each(function() {
        $(this).contents().filter(function () {
            return $(this).prop('tagName') !== 'A';
        }).remove();

        var newElement = $("<div />")
            .addClass('fotorama')
            .attr('data-nav', 'thumbs')
            .attr('data-allowfullscreen', 'true')
            .attr('data-minwidth', '100%')
            .attr('data-maxwidth', '100%')
            .attr('data-maxheight', '400px');

        $.each(this.attributes, function(i, attrib){
            $(newElement).attr(attrib.name, attrib.value);
        });

        $(this).replaceWith(function () {
            return $(newElement).append($(this).contents());
        });
    });

    youtubes = $result.find('youtube').toArray();
    for (i in youtubes) {
        e = $(youtubes[i]);

        var start = 0;

        var m = /[0-9]+(?=m)/.exec(e.attr('t'));
        if (m !== null) {
            start += parseInt(m[0]) * 60;
        }

        var s = /[0-9]+(?=s)/.exec(e.attr('t'));
        if (s !== null) {
            start += parseInt(s[0]);
        }

        $(e).replaceWith($(`<div class="aspectRatio_16-9"><iframe src="https://www.youtube-nocookie.com/embed/${e.attr('v')}?start=${start}&list=${e.attr('list')}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>`));
    }

    setTimeout(function () {
        tryLazyload();
        $('.fotorama').fotorama();
    }, 100);

    return $result[0].innerHTML
        .replace(/_src_/g, 'src');
}

async function mdRenderAmp(text) {
    var $result = $('<div/>', {html: simplemde.markdown(text)
        .replace(/src/g, '_src_')});

    imgs = $result.find('img').toArray();
    for (i in imgs) {
        e = $(imgs[i]);

        var src = e.attr('_src_');
        if (typeof src === 'string') {

            var newElement = $('<amp-img></amp-img>');
            var img = $('<img/>');

            newElement.attr('layout', 'responsive')

            if (src.substr(0, 5) == '/img/') {
                src = src.substr(0, src.lastIndexOf('.')) + '-1920.jpg';
            }

            newElement.attr('_src_', src);
            img.attr('_src_', src);

            var alt = e.alt;
            if (typeof alt === 'string') {
                newElement.attr('alt', alt);
                img.attr('alt', alt);
            }

            try {
                imgC = await loadImg(newElement.attr('_src_'));

                newElement.attr('width', imgC.width);
                img.attr('width', imgC.width);
                newElement.attr('height', imgC.height);
                img.attr('height', imgC.height);
            } catch (e) {
                console.warn(e);

                $('#toast')[0].MaterialSnackbar.showSnackbar({'message': `Bild ${e.message} konnte nicht gefunden werden.`});
            }

            if (e.parent().is('gallery')) {
                newElement.attr('lightbox', '')
                    .append($('<noscript/>').append(img));
                e.replaceWith(newElement);
            } else {
                newElement.append($('<noscript/>').append(img));
                e.replaceWith($('<amp-carousel/>')
                    .attr('type', 'slides')
                    .attr('lightbox', '')
                    .attr('layout', 'responsive')
                    .attr('width', newElement.attr('width'))
                    .attr('height', newElement.attr('height'))
                    .append(newElement));
            }

            $('#editArticle_progressbar').get(0).MaterialProgress.setProgress((.5 * i / imgs.length) * 100);
        }
    }



    galleries = $result.find('gallery').toArray();
    for (i in galleries) {
        e = $(galleries[i]);

        if (e.children().length > 0) {
            imgs = e.find('amp-img').toArray();

            var width = 0;
            var height = 0;

            for (imgI in imgs) {
                var img = $(imgs[imgI]);

                if (img.attr('width') > width) {
                    width = img.attr('width');
                }
                if (img.attr('height') > height) {
                    height = img.attr('height');
                }
            }

            var newGallery = $('<amp-carousel/>')
                .attr('id', `slides${i}`)
                .attr('controls', '')
                .attr('insertbracketopenslideinsertbracketclose', `selectedSlide${i}`)
                .attr('on', `slideChange:AMP.setState({selectedSlide${i}: event.index})`)
                .attr('layout', 'responsive')
                .attr('type', 'slides')
                .attr('width', width)
                .attr('height', height)
                .attr('lightbox', '');

            var newSlider = $('<amp-carousel/>')
                .attr('width', 'auto')
                .attr('height', 48)
                .attr('layout', 'fixed-height')
                .attr('type', 'carousel');

            var newCounter = $(`<p class='galleryCounter'><span insertbracketopentextinsertbracketclose="+selectedSlide${i} + 1">1</span>/${imgs.length}</p>`)

            for (imgI in imgs) {
                var img = $(imgs[imgI]);

                newGallery.append($('<amp-img/>')
                    .attr('_src_', img.attr('_src_'))
                    .attr('height', img.attr('height'))
                    .attr('width', img.attr('width'))
                    .attr('alt', img.attr('alt'))
                    .attr('layout', img.attr('layout'))
                    .append(img.find('noscript')));

                var src = img.attr('_src_');
                var width = img.attr('width');
                var height = img.attr('height');

                if (src.slice(0, 5) === '/img/' && src.slice(-9) === '-1920.jpg') {
                    src = src.slice(0, -9) + '-100.jpg';

                    try {
                        imgC = await loadImg(src);

                        width = imgC.width;
                        height = imgC.height;
                    } catch (e) {
                        console.warn(e);

                        $('#toast')[0].MaterialSnackbar.showSnackbar({'message': `Bild ${e.message} konnte nicht gefunden werden.`});
                    }
                }

                newSlider.append($('<button/>')
                    .attr('on', `tap:slides${i}.goToSlide(index=${imgI})`)
                    .append(img
                        .attr('_src_', src)
                        .attr('width', width)
                        .attr('height', height)
                        .attr('lightbox', null)));

                $('#editArticle_progressbar').get(0).MaterialProgress.setProgress((.5 + .5 * imgI / imgs.length) * 100);
            }

            newSlider.insertAfter(e);
            newCounter.insertAfter(e);
            newGallery.insertAfter(e);
        }

        e.remove();
    }

    youtubes = $result.find('youtube').toArray();
    for (i in youtubes) {
        e = $(youtubes[i]);

        var start = 0;

        var m = /[0-9]+(?=m)/.exec(e.attr('t'));
        if (m !== null) {
            start += parseInt(m[0]) * 60;
        }

        var s = /[0-9]+(?=s)/.exec(e.attr('t'));
        if (s !== null) {
            start += parseInt(s[0]);
        }

        $(e).replaceWith($(`<amp-iframe width="750" height="422" layout="responsive" src="https://www.youtube-nocookie.com/embed/${e.attr('v')}?start=${start}&list=${e.attr('list')}" frameborder="0" allowfullscreen sandbox="allow-scripts allow-same-origin"><div placeholder class="amp-wp-iframe-placeholder"></div></amp-iframe>`));
    }

    result = $result[0].innerHTML
        .replace(/insertbracketopen/g, '[')
        .replace(/insertbracketclose/g, ']')
        .replace(/_src_/g, 'src');

    return result;
}

async function saveArticle() {
    $('#saveButton').prop('disabled', true);

    $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Speichern, bitte warten...'});

    $('#editArticle_progressbar').get(0).MaterialProgress.setProgress(0);
    $('#editArticle_progressbar').toggleClass('active', true);

    var elements = {
        'slug': {'id': 'slug', 'val': function () {
            return $('#slug').val();
        }},
        'headline': {'id': 'h1', 'val': function () {
            return $('#h1').val();
        }},
        'subline': {'id': 'h3', 'val': function () {
            return $('#h3').val();
        }},
        'text': {'id': 'text', 'val': function () {
            return mdRender(simplemde.value());
        }},
        'textAmp': {'id': 'text', 'val': function () {
            return mdRenderAmp(simplemde.value());
        }},
        'textMd': {'id': 'text', 'val': function () {
            return simplemde.value();
        }},
        'author': {'id': 'author', 'val': function () {
            return $('#author').val();
        }},
        'date': {'id': 'date', 'val': function () {
            return (new Date($('#date').val()).getTime() / 1000);
        }},
        'categoryId': {'id': 'category', 'val': function () {
            return Number($('#category').val());
        }},
        'hidden': {'id': 'hidden', 'val': function () {
            return $('#hidden').prop('checked');
        }},
        'private': {'id': 'private', 'val': function () {
            return $('#private').prop('checked');
        }}
    };

    var data = {};

    for (key in elements) {
        data[key] = await elements[key].val();
    }

    $('#editArticle_progressbar').get(0).MaterialProgress.setProgress(100);
    $('#editArticle_progressbar').toggleClass('mdl-progress__indeterminate', true);

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: '/ajax/saveArticle/' + articleId,
        data: JSON.stringify(data),
        processData: false,
        contentType: "application/json; charset=utf-8",
        success: function(data, textStatus, jqxhr){
            for (key in elements) {
                $('#' + elements[key].id).removeClass('error');
                $('div.errorText[for=' + elements[key].id + ']').html('');
            }

            $('#saveButton').prop('disabled', false);

            $('#editArticle_progressbar').toggleClass('mdl-progress__indeterminate', false);
            $('#editArticle_progressbar').toggleClass('active', false);

            if (data.success) {
                $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Gespeichert'});

                for (key in data.removedImgs) {
                    $('#toast')[0].MaterialSnackbar.showSnackbar({'message': data.removedImgs[key] + ' wurde entfernt, da es nicht verwendet wurde'});
                }

                $('#removeButton').removeClass('hidden');
            } else {
                $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Fehler'});

                for (var key in data.error) {
                    $('#' + elements[key].id).addClass('error');
                    $('div.errorText[for=' + elements[key].id + ']').html(data.error[key]);
                }
            }
        },
        error: function(jqxhr, textStatus, errorThrown){
            $('#saveButton').prop('disabled', false);

            $('#editArticle_progressbar').toggleClass('mdl-progress__indeterminate', false);
            $('#editArticle_progressbar').toggleClass('active', false);

            swal(errorThrown, $('<div/>').text(JSON.stringify(jqxhr)).html());
        }
    });
}

function removeArticle() {
    swal({
        title: 'Sicher?',
        html: 'Willst du den Eintrag <b>wirklich</b> löschen ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja',
        cancelButtonText: 'Nein',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    dataType: "json",
                    url: '/ajax/removeArticle/' + articleId,
                    processData: false,
                    success: function (data, textStatus, jqxhr) {
                        if (data.success) {
                            resolve();
                        } else {
                            reject('Serverfehler: ' + JSON.stringify(data));
                        }
                    },
                    error: function (jqxhr, textStatus, errorThrown) {
                        reject('Kommunikationsfehler: ' + errorThrown);
                    }
                });
            })
        },
        allowOutsideClick: false
    }).then(function () {
        swal({
            type: 'success',
            title: 'Gelöscht',
            html: 'Du wirst nun auf die Startseite weitergeleitet werden.'
        }).then(function () {
            location = '/';
        }, function () {
            location = '/';
        });
    }, function () {});
}

function saveCategory() {
    var elements = {
        'slug': {'id': 'slug', 'val': function () {
            return $('#slug').val();
        }},
        'name': {'id': 'name', 'val': function () {
            return $('#name').val();
        }}
    };

    var data = {};

    for (key in elements) {
        data[key] = elements[key].val();
    }

    $('#saveButton').prop('disabled', true);

    $.ajax({
        type: 'POST',
        dataType: "json",
        url: '/ajax/saveCategory/' + categoryId,
        data: JSON.stringify(data),
        processData: false,
        contentType: "application/json; charset=utf-8",
        success: function(data, textStatus, jqxhr){
            for (key in elements) {
                $('#' + elements[key].id).removeClass('error');
                $('div.errorText[for=' + elements[key].id + ']').html('');
            }

            $('#saveButton').prop('disabled', false);

            if (data.success) {
                $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Gespeichert'});

                $('#removeButton').removeClass('hidden');
            } else {
                $('#toast')[0].MaterialSnackbar.showSnackbar({'message': 'Fehler'});

                for (var key in data.error) {
                    $('#' + elements[key].id).addClass('error');
                    $('div.errorText[for=' + elements[key].id + ']').html(data.error[key]);
                }
            }
        },
        error: function(jqxhr, textStatus, errorThrown){
            $('#saveButton').prop('disabled', false);

            swal(errorThrown, $('<div/>').text(JSON.stringify(jqxhr)).html());
        }
    });
}

function removeCategory() {
    var articleList = '<div>'
    for (key in articles) {
        article = articles[key];
        articleList += '<div><a href="/article/' + article.slug + '">';
        articleList += article.headline;
        articleList += '</a></div>';
    }
    articleList += '</div>'

    swal({
        title: 'Sicher?',
        html: 'Willst du die Kategorie und die beinhalteten Artikel <b>wirklich</b> löschen ?<br>' + articleList,
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ja',
        cancelButtonText: 'Nein',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        showLoaderOnConfirm: true,
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                $.ajax({
                    dataType: "json",
                    url: '/ajax/removeCategory/' + categoryId,
                    processData: false,
                    success: function (data, textStatus, jqxhr) {
                        if (data.success) {
                            resolve();
                        } else {
                            reject('Serverfehler: ' + JSON.stringify(data));
                        }
                    },
                    error: function (jqxhr, textStatus, errorThrown) {
                        reject('Kommunikationsfehler: ' + errorThrown);
                    }
                });
            })
        },
        allowOutsideClick: false
    }).then(function () {
        swal({
            type: 'success',
            title: 'Gelöscht',
            html: 'Du wirst nun auf die Startseite weitergeleitet werden.'
        }).then(function () {
            location = '/';
        }, function () {
            location = '/';
        });
    }, function () {});
}

function login() {
    swal({
        title: 'Login',
        html: '<div id="firebaseauth"></div>',
        confirmButtonColor: '#d33',
        confirmButtonText: 'Abbrechen',
        confirmButtonClass: 'btn btn-danger',
        onOpen: function () {
            window.location.hash = 'login';
            ui = new firebaseui.auth.AuthUI(firebase.auth());
            ui.start('#firebaseauth', uiConfig);
        }
    }).then(function () {
        ui.delete();
        window.location.hash = '';
    }, function () {
        ui.delete();
        window.location.hash = '';
    });
}

function logout() {
    Cookies.remove('token', {
        domain: domain
    });
    Cookies.remove('localStorage', {
        domain: domain
    });
    localStorage.clear();
    firebase.auth().signOut().then(function() {
        location.reload();
    }, function(error) {
        swal('Fehler', 'Du konntest nicht ausgeloggt werden.<br>' + error, 'error').then(function () {
            location.reload();
        });
    });
}

function reauth() {
    // refresh firebase token
    if (firebase.auth().currentUser) {
        // reauthenticate
        firebase
        .auth()
        .currentUser.getIdToken(true)
        .then(function (accessToken) {
            Cookies.set('token', accessToken, {
                domain: domain
            });
            Cookies.set('localStorage', JSON.stringify(localStorage), {
                domain: domain
            });
        });
    } else {
        // vom localStorage der anderen Seite ziehen
        if (!Cookies.get('localStorage', {
            domain: domain
        })) {
            return;
        }
        var lStorageStuff = JSON.parse(Cookies.get('localStorage', {
            domain: domain
        }))
        for (var key in lStorageStuff) {
            if (lStorageStuff.hasOwnProperty(key)) {
                localStorage.setItem(key, lStorageStuff[key]);
            }
        }
    }
}

function search(elemID) {
    var elem = document.getElementById(elemID);
    if (elem.value.length > 0) {
        location.href = videoPlatformLink + "/search?q=" + elem.value;
    }
}

var xhr;
function autoCompleteElement(elemID) {
    new autoComplete({
        selector: '#' + elemID,
        source: function (term, response) {
            try {
                xhr.abort();
            } catch (e) {}
            xhr = $.getJSON(videoPlatformLink + '/autocomplete_blog', {
                term: term
            }, function (data) {
                response(data);
            });
        },
        minChars: 1,
        renderItem: function (item, search) {
            console.log(item)
            return '<div class="autocomplete-suggestion" data-val="' + item.slice(1) + '">' + item.slice(1) + '</div>';
        }
    });
}

function rssSettings(categories) {
    var html = $('<div id="rssSettings"/>');

    html.append($( `<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="all">
                        <input type="checkbox" id="all" class="mdl-checkbox__input" checked onchange="rssSettingsGuiUpdate()">
                        <span class="mdl-checkbox__label">Alle</span>
                    </label>`));

    var categoryList = $('<div class="categoryList"/>');
    for (i in categories) {
        var category = categories[i];

        categoryList.append($( `<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="rssSettingsCheckbox_${category.slug}">
                                    <input type="checkbox" id="rssSettingsCheckbox_${category.slug}" slug="${category.slug}" class="mdl-checkbox__input" checked onchange="rssSettingsGuiUpdate()">
                                    <span class="mdl-checkbox__label">${category.name}</span>
                                </label>`));
    }

    html.append(categoryList);

    html.append($( '<div class="flexInputContainer">\
                        <div class="mdl-textfield mdl-js-textfield">\
                            <input id="rssSettingsInput" class="mdl-textfield__input" type="text" readonly onchange="rssSettingsGuiUpdate()">\
                        </div>\
                        <a class="hiddenA" onclick="selectCopy($(\'#rssSettingsInput\')[0])">\
                            <i class="material-icons">content_copy</i>\
                        </a>\
                        <a class="hiddenA" onclick="window.open($(\'#rssSettingsInput\').prop(\'value\'), \'_blank\')">\
                            <i class="material-icons">open_in_new</i>\
                        </a>\
                    </div>'));

    swal({
        title: 'RSS Link Generator',
        html: html,
        confirmButtonColor: '#d33',
        confirmButtonText: 'Abbrechen',
        confirmButtonClass: 'btn btn-danger',
        onOpen: function() {
            componentHandler.upgradeDom();
            rssSettingsGuiUpdate();
        }
    });
}

function rssSettingsGuiUpdate() {
    var url = location.protocol + '//' + location.host;

    if (location.port !== '') {
        url += ':' + location.port;
    }

    url += '/rss';

    $('#rssSettings .categoryList').toggleClass('active', !$('#rssSettings #all').prop('checked'));

    if (!$('#rssSettings #all').prop('checked')) {
        url += '?c=';

        $('#rssSettings .categoryList input').each(function (e) {
            var box = $($('#rssSettings .categoryList input')[e]);

            if (box.prop('checked')) {
                url += box.attr('slug') + ',';
            }
        });

        if (url.slice(-1) === ',') {
            url = url.slice(0, -1);
        } else {
            url += '-1';
        }
    }

    $('#rssSettingsInput').prop('value', url);
}

function selectAll(e) {
    e.focus();
    e.setSelectionRange(0, e.value.length);
}

function selectCopy(e) {
    selectAll(e);
    document.execCommand('copy');
}

function newsSettings(categories, activeCategories, token) {
    var html = $('<div id="newsSettings"/>');

    var checked = activeCategories.includes('-1') ? ' checked' : '';

    html.append($( `<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="all">
                        <input type="checkbox" id="all" class="mdl-checkbox__input"${checked} onchange="newsSettingsGuiUpdate()">
                        <span class="mdl-checkbox__label">Alle</span>
                    </label>`));

    var categoryList = $('<div class="categoryList"/>');
    for (i in categories) {
        var category = categories[i];

        var checked = activeCategories.includes(category.slug) || activeCategories.includes('-1') ? ' checked' : '';

        categoryList.append($( `<label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="newsSettingsCheckbox_${category.slug}">
                                    <input type="checkbox" id="newsSettingsCheckbox_${category.slug}" slug="${category.slug}" class="mdl-checkbox__input"${checked} onchange="newsSettingsGuiUpdate()">
                                    <span class="mdl-checkbox__label">${category.name}</span>
                                </label>`));
    }

    html.append(categoryList);

    swal({
        title: 'Newsletter Einstellungen',
        html: html,
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Speichern',
        cancelButtonText: 'Abbrechen',
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        showLoaderOnConfirm: true,
        onOpen: function() {
            componentHandler.upgradeDom();
            newsSettingsGuiUpdate();
        },
        preConfirm: function () {
            return new Promise(function (resolve, reject) {
                if ($('#newsSettings #all').prop('checked')) {
                    var toActivateCategories = [-1]
                } else {
                    var toActivateCategories = []

                    $('#newsSettings .categoryList input').each(function (e) {
                        var box = $($('#newsSettings .categoryList input')[e]);

                        if (box.prop('checked')) {
                            toActivateCategories.push(box.attr('slug'));
                        }
                    });
                }

                $.ajax({
                    dataType: "json",
                    url: '/ajax/setNews',
                    method: 'POST',
                    data: {'token': token, 'toActivateCategories': toActivateCategories},
                    success: function (data, textStatus, jqxhr) {
                        if (data.success) {
                            resolve();
                        } else {
                            reject('Fehler: ' + data.error);
                        }
                    },
                    error: function (jqxhr, textStatus, errorThrown) {
                        reject('Fehler: ' + JSON.parse(jqxhr.responseText).error + ' (' + errorThrown + ')');
                    }
                });
            })
        }
    }).then(function () {
        swal({
            type: 'success',
            title: 'Gespeichert',
            html: null
        }).then(function () { location.reload(); },
                function () { location.reload(); });
    }, function () {});
}

function newsSettingsGuiUpdate() {
    $('#newsSettings .categoryList').toggleClass('active', !$('#newsSettings #all').prop('checked'));
}

function getParameterByName(name, url) { // https://stackoverflow.com/a/901144
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return '';
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
