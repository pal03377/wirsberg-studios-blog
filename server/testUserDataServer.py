from flask import *
import json

app = Flask(__name__)

COMMUNICATION_SECRET = "secret"

users = {
   "0": {
       "id": "000gdsfhjklsdfkjhg",
       "name": "Tilman",
       "email": "info@tc-j.de",
       "role": 99
   },
   "1": {
       "id": "111sdgfhjibgfd",
       "name": "Paul",
       "email": "hallo@welt.de",
       "role": 20
   },
   "2": {
       "id": "222dufhgztoips",
       "name": "Nemo",
       "email": "niemand@hier.de",
       "role": 0
   }
}

@app.route("/login", methods=["GET", "POST"])
def login():
    print("/login, get params:", request.args)
    print("/login, post params:", request.form)
    if request.method == "GET":
        return """
<form action="/login" method="POST" id="f">
<input name="userid" placeholder="user id" type="text">
<button type="submit" name="action">OK</button>
</form>
<script>
document.getElementById("f").action = location.href;
</script>
"""
    else:
        userID = request.form["userid"]
        token = "TOKEN_" + userID # das echte Token ist unberechenbarer! ;-)
        if request.args.get("redirect_subdomain") == "blog":
            # eigentlich wird der Inhalt von request.args.get(currentPagePath) noch überprüft...
            print("redirect to [blog]/" + request.args.get("currentPagePath"), "...")
            print("... with cookie header token =", token)
            r = redirect("http://127.0.0.1/" + request.args.get("currentPagePath"))
            response = current_app.make_response(r)
            response.set_cookie('token', value=token, domain="127.0.0.1")
            return response
        else:
            return "you are logged in. Token: " + token

@app.route("/getUserData/<communicationSecret>", methods=["POST"])
def getUserData(communicationSecret):
    print("/getUserData")
    if communicationSecret != COMMUNICATION_SECRET:
        print("wrong comm secret,", communicationSecret, "instead of", COMMUNICATION_SECRET)
        return "wrong comm secret", 403
    token = request.form["token"]
    print("token:", token)
    if token.replace("TOKEN_", "") not in users:
        return "wrong token", 403
    user = users[token.replace("TOKEN_", "")]
    return json.dumps({
        "id": user["id"],
        "name": user["name"],
        "email": user["email"],
        "role": user["role"]
    })

if __name__ == "__main__":
    app.run("127.0.0.1", port=7357, debug=True)
