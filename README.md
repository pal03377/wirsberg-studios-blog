# Wirsberg Studios Blog

## db structure:
- articles
 + slug
 + headline
 + subline
 + text
 + textMd
 + author
 + date
 + categoryId
 + hidden
 + private
- categories
 + slug
 + name
- newsReceivers
 + userId
 + name
 + email
 + rmToken
- newsletterByCategory
 + userId
 + categoryId
